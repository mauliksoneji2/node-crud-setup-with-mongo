const express = require("express");
const { getContacts, createContact, getContactById, updateContactById, deleteContactById } = require("../controllers/contact.controller");
const validateToken = require("../middleware/validate");
const router = express.Router();

router.use(validateToken)
router.route("/").get(getContacts).post(createContact);


router
  .route("/:id")
  .get(getContactById)
  .put(updateContactById)
  .delete(deleteContactById);


module.exports= router