const asyncHandler = require("express-async-handler");
const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

//@desc Register a User
//@routR POST /api/users/register
//@access public
const registerUser = asyncHandler(async (req, res) => {
  const { email, username, password } = req.body;
  if (!email || !username || !password) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }
  const userAvailable = await User.findOne({ email });
  if (userAvailable) {
    res.status(400);
    throw new Error("User already registered");
  }
  // Hash password
  const hashPassword = await bcrypt.hash(password, 10);
  const user = await User.create({
    email,
    username,
    password: hashPassword,
  });
  if (user) {
    res.status(201).json({ _id: user.id, email: user.email });
  } else {
    res.status(400);
    throw new Error("User data is not valid");
  }
  res.status(200).json({ message: "Registered user successfully", user });
});

//@desc Login a User
//@routR POST /api/users/login
//@access private
const loginUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }
  const user = await User.findOne({
    email,
  });
    //compare password with hashpassword
    if (user && (await bcrypt.compare(password, user.password))) {
        const accessToken = jwt.sign(
          {
            user: {
              username: user.username,
              email: user.email,
              id: user.id,
            },
          },
            process.env.ACCESS_TOKEN_SECRET,
            {
              expiresIn:'15m'
          }
        );
        res.status(200).json({accessToken})
    } else {
        res.status(401)
        throw new Error("email or password not valid")
    }
});

//@desc Current User
//@routR POST /api/users/current
//@access public
const currentUser = asyncHandler(async (req, res) => {
  //     const contacts = await User.create({

  //   });
  res.status(200).json(req.user);
});

module.exports = { registerUser, loginUser, currentUser };
