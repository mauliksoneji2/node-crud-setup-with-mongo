const asyncHandler = require("express-async-handler");
const Contact = require("../models/contact.model")

//@desc Get all contacts
//@route GET /api/contacts/
//@access public
const getContacts = asyncHandler(async (req, res) => {
  const contacts = await Contact.find({user_id:req.user.id});
  res.status(200).json({ message: "Get all contacts",contacts });
});

//@desc Create contacts
//@route POST /api/contacts/
//@access private
const createContact = asyncHandler (async (req, res) => {
  const { email, name, phone } = req.body;
  if (!email || !name || !phone) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }
  const contact = await Contact.create({
    email,name,phone, user_id:req.user.id
  })
  res.status(200).json({ message: "Create contact" ,contact});
});

//@desc Get Contact
//@route GET /api/contacts/id
//@access private
const getContactById = asyncHandler(async (req, res) => {
  const contact = await Contact.findById(req.params.id)
  if (!contact) {
    res.status(404);
    throw new Error("Contact not found")
  }
  res.status(200).json({contact});
});

//@desc Update Contact
//@route PUT /api/contacts/id
//@access private
const updateContactById = asyncHandler(async (req, res) => {
  const contact = await Contact.findById(req.params.id);
  if (!contact) {
    res.status(404);
    throw new Error("Contact not found");
  }

  if (contact.user_id.toString()!== req.user.id) {
    res.status(403);
    throw new Error("User don't have permission to update contact")
  }

  const updatedContact = await Contact.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      new: true
    }
  );

  res
    .status(200)
    .json({
      message: `Update contact for ${req.params.id}`,
      contact: updatedContact,
    });
});

//@desc Delete Contact
//@route DELETE /api/contacts/id
//@access private
const deleteContactById = asyncHandler(async (req, res) => {
  const contact = await Contact.findById(req.params.id);
  if (!contact) {
    res.status(404);
    throw new Error("Contact not found");
  }
   await Contact.deleteOne({_id:req.params.id});
  res.status(200).json({ message: `Delete contact for ${req.params.id}`,contact });
});

module.exports={getContacts, createContact, getContactById, updateContactById, deleteContactById}